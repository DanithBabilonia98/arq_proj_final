# Create your tasks here
from arq_proj_final.celery import app
from bs4 import BeautifulSoup
import requests
from .models import Product

@app.task
def get_prices():
    url = 'https://www.dafiti.com.co/masculino/vestuario/vestuario-camisetas-masculino/'

    result = requests.get(url)
    doc = BeautifulSoup(result.text, "html.parser")
    title_p = doc.find_all(class_="itm-brand", limit=10)
    names = doc.find_all(class_="itm-title", limit=10)
    prices = doc.find_all(class_='itm-price special original-price', limit=10)
    title = []
    name = []
    price =[]


    for t in title_p:
        title.append(t.text)
    
    for n in (names):
        name.append(n.text)

    
    for p in prices:
        price_new = str(p.text)
        price_corrected = int((price_new).replace("$","").replace(".",""))
        price.append(price_corrected)

    product = []
    for name_price in zip(title,name,price):
        t_shirt = {
            'title': name_price[0],
            'name': name_price[1],
            'price': name_price[2],
        }
        product.append(t_shirt) 
        
        created = Product.objects.update_or_create(
            title = name_price[0],
            name = name_price[1],
            price = name_price[2],
        )


    return product
