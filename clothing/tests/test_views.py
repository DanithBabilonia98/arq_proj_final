from django.test import TestCase
from django.urls import reverse_lazy
from django.contrib.auth import get_user_model


User = get_user_model()


class Test_Api_Rest(TestCase):
    url = 'http://127.0.0.1:8000/clothing/'
    url2 = 'http://127.0.0.1:8000/clothing/1/'
    
    def test_login(self):
        user = User.objects.create_user(username='test', password='test')
        self.client.login(username='test', password='test')
        request_get = self.client.get(self.url)
        print(f'login {request_get.status_code}')
        self.assertEqual(request_get.status_code, 200)
        
        
    def test_login_invalid(self):
        user = User.objects.create_user(username='test', password='test')
        try_login = self.client.login(username='monica', password='monica')
        print(f'login invalid {try_login}')
        self.assertEqual(try_login, False)
        
    #post
    def test_login_post(self):
        user = User.objects.create_user(username='test', password='test')
        self.client.login(username='test', password='test')
        request_post = self.client.post(self.url, { 
                                                    "title": "a",
                                                    "name": "a",
                                                    "price": 10 ,
                                                    })
        print(f'post {request_post.status_code}')
        self.assertEqual(request_post.status_code, 201)
    #put
    def test_login_put(self):
        data1 = { 
                    "title": "a",
                    "name": "a",
                    "price": 10 ,
        }
        
        data2 = { 
                    "title": "b",
                    "name": "b",
                    "price": 20 ,
                }
        user = User.objects.create_user(username='test', password='test')
        self.client.login(username='test', password='test')
        request_post = self.client.post(self.url, data=data1)
        request_put = self.client.put(self.url2, data=data2, content_type = 'application/json')
        print(f'put {request_put.status_code}')
        self.assertEqual(request_put.status_code, 200)

    #delete
    def test_login_delete(self):
        data1 = { 
                    "title": "a",
                    "name": "a",
                    "price": 10 ,
        }
        user = User.objects.create_user(username='test', password='test')
        self.client.login(username='test', password='test')
        request_post = self.client.post(self.url, data=data1)
        request_delete = self.client.delete(self.url2)
        print(f'delete {request_delete.status_code}')
        self.assertEqual(request_delete.status_code, 204)
    #delete owner
    def test_login_delete_owner(self):
        data1 = { 
                    "title": "a",
                    "name": "a",
                    "price": 10 ,
        }
        user = User.objects.create_user(username='test', password='test')
        self.client.login(username='test', password='test')
        request_post = self.client.post(self.url, data=data1)
        self.client.logout()
        user = User.objects.create_user(username='test2', password='test2+')
        self.client.login(username='test2', password='test2')
        request_delete_owner = self.client.delete(self.url2)
        
        print(f'delete_not_owner {request_delete_owner.status_code}')
        self.assertEqual(request_delete_owner.status_code,403)
        
    #logout
    def test_logout(self):
        data1 = { 
                    "title": "a",
                    "name": "a",
                    "price": 10 ,
        }
        user = User.objects.create_user(username='test', password='test')
        self.client.login(username='test', password='test')
        self.client.logout()
        request_post_logout = self.client.post(self.url, data=data1)
        print(f'logout {request_post_logout.status_code}')
        self.assertEqual(request_post_logout.status_code,403)
        