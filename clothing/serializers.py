from .models import Product
from rest_framework import serializers


class ClothingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = ['id','title','name', 'price']

