from django.shortcuts import render
from .models import Product
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import ClothingSerializer

# Create your views here.
class ClothingViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Product.objects.all().order_by('price')
    serializer_class = ClothingSerializer
    permission_classes = [permissions.IsAuthenticated]