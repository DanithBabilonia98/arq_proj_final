# Final Archictecture Project

## Dependencies
* Django 4.0.4
* celery 5.2.6
* requests 2.27.1
* beautifulsoup4 4.11.1
* djangorestframework 3.13.1
* coverage 6.3.2
* flower 1.0.0

## Installation and Configuration

## Clone the repository Project and create virtual environment
![readme](doc/images/env.png)

## Activate the virtual environment
![readme](doc/images/activate.png)

## Setup:

* pip install -r requirements.txt
* python manage.py migrate
* python manage.py createsuperuser
* pytthon manage.py runserver

![readme](doc/images/celery.png)
![readme](doc/images/celery2.png)
![readme](doc/images/celery3.png)

## Coverage

![readme](doc/images/cov.png)
![readme](doc/images/cov2.png)


## Celery

![readme](doc/images/celery.png)

![readme](doc/images/celery2.png)

![readme](doc/images/celery3.png)

## Project Run

* python manage.py runserver 

![readme](doc/images/run.png)

![readme](doc/images/login.png)

![readme](doc/images/ima2.png)

![readme](doc/images/run2.png)
